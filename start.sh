#!/bin/bash
#cd /home/alexandr/block
##git fetch --all
#sudo git reset --hard origin/main
sudo apt install npm
sudo chown root:root *
sudo chmod -R 700 log
sudo cp blocklog /etc/logrotate.d/blocklog
sudo logrotate -v -f /etc/logrotate.d/blocklog
wget -q https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb
sudo dpkg -i cloudflared-linux-amd64.deb
sudo apt install haproxy -y
sudo groupadd -f testers
sudo groupadd -f developers
sudo groupadd -f devops
sudo useradd -g developers -p qwerty developers 
sudo chown developers:developers ./log
sudo rm tmp/*
sudo mkdir -p /mainlogs
sudo touch /mainlogs/ping.log
sudo chmod -R 777 /mainlogs
ping -c 4 127.0.0.1 >> /mainlogs/ping.log
ping -c 4 127.0.0.2 > /mainlogs/ping.log
ping -c 4 127.0.0.3 > /mainlogs/ping.log
sudo tar -cvf /newtarball.tar *
